//
//  Util.swift
//  youtube_list
//
//  Created by MacBook on 21/7/2561 BE.
//  Copyright © 2561 sptop. All rights reserved.
//

import UIKit




class Util {
    public static func getVideoID(string:String)->String?{
        guard let components = URLComponents(url: URL(string:string)!, resolvingAgainstBaseURL: true), let queryItems = components.queryItems else {
            return nil
        }
        
        var parameters = [String: String]()
        for item in queryItems {
            parameters[item.name] = item.value
        }
        
        return parameters["v"];
    }
}
