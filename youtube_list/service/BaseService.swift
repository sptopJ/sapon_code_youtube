//
//  BaseService.swift
//  youtube_list
//
//  Created by MacBook on 20/7/2561 BE.
//  Copyright © 2561 sptop. All rights reserved.
//

import UIKit

class BaseService: NSObject {
    
    enum HttpError: Error {
        case unknownError
        case connectionError
        case invalidCredentials
        case invalidRequest
        case notFound
        case invalidResponse
        case serverError
        case serverUnavailable
        case timeOut
        case unsuppotedURL
    }
    
   public static func loadDataFromURL(url: URL, completion: @escaping (_ receive_data: Data? , _ error:Error?) -> Void) {
    
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard data != nil else {
                let response = response as? HTTPURLResponse
                if response?.statusCode != 200 {
                    print("http status code: \(String(describing: response?.statusCode))")
                    completion(nil,HttpError.connectionError);
                }else if(error != nil){
                    completion(nil,error);
                }
                return
            }
            completion(data,nil);
        }
        task.resume()
        
    }
    
    
    
}
