//
//  Data.swift
//  youtube_list
//
//  Created by MacBook on 20/7/2561 BE.
//  Copyright © 2561 sptop. All rights reserved.
//

import UIKit

class Playlist: NSObject {
    public var list_items:Array = [Item]();
    public var list_title:String = "";
    init?(json: [String: Any]) {
        
        if let list_title = json["list_title"] as? String {
            self.list_title = list_title;
        }
        
        guard let list_items = json["list_items"] as? Array<Dictionary<String,Any>>
            else
        {
            return nil ;
        }
        
        for item in list_items {
            if  let item_obj = Item(json: item){
                 self.list_items.append(item_obj);
            }
        }
    }
    
    
    
}
