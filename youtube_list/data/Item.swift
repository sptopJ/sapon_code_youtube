//
//  Item.swift
//  youtube_list
//
//  Created by MacBook on 20/7/2561 BE.
//  Copyright © 2561 sptop. All rights reserved.
//

import UIKit

class Item: NSObject {
    public var title:String = "";
    public var link:String = "";
    public var thumb:String = "";
    
    
    init?(json: [String: Any]) {
        guard let title = json["title"] as? String,
            let link = json["link"] as? String,
            let thumb = json["thumb"] as? String
        else{
                return nil;
        }
        
        self.title = title;
        self.link = link;
        self.thumb = thumb;
        
        
        
    }
    
    
}
