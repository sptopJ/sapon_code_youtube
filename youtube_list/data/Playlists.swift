//
//  VideoList.swift
//  youtube_list
//
//  Created by MacBook on 20/7/2561 BE.
//  Copyright © 2561 sptop. All rights reserved.
//

import UIKit

class Playlists: NSObject {
    public var list = [Playlist]();
    
    init?(json: [String: Any]) {
        guard let playlists = json["playlists"] as? Array<Dictionary<String,Any>>
            else
        {
            return nil ;
        }
        
        for playlist_json in playlists {
            if let playlist = Playlist(json: playlist_json){
                self.list.append(playlist);
            }
        }
        
    }
    
}
