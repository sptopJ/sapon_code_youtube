//
//  PlaylistViewController.swift
//  youtube_list
//
//  Created by MacBook on 20/7/2561 BE.
//  Copyright © 2561 sptop. All rights reserved.
//

import UIKit
import youtube_ios_player_helper;


class PlaylistViewController: UIViewController , YTPlayerViewDelegate {

    
    public var playlist:Playlist?;
    
    @IBOutlet weak var playlist_tableview: UITableView!
    
    let ITEM_TABLEVIEWCELL = "ItemTableViewCell";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let uiNib :UINib = UINib(nibName: ITEM_TABLEVIEWCELL, bundle: nil)
        playlist_tableview.register(uiNib, forCellReuseIdentifier: ITEM_TABLEVIEWCELL)
        
        self.playlist_tableview.dataSource = self;
        self.playlist_tableview.delegate = self;
        self.playlist_tableview.reloadData();
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
        
    
    }

}



extension PlaylistViewController: UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(playlist != nil){
            return playlist!.list_items.count
        }else{
            return 0;
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ITEM_TABLEVIEWCELL)! as! ItemTableViewCell;
        let item = playlist!.list_items[indexPath.row];
        //buffer video and set row data
        cell.setItem(item: item);
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? ItemTableViewCell{
            cell.playVideo();
        }
    }
    
}
