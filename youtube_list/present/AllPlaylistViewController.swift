//
//  AllPlaylistViewController.swift
//  youtube_list
//
//  Created by MacBook on 20/7/2561 BE.
//  Copyright © 2561 sptop. All rights reserved.
//

import UIKit

class AllPlaylistViewController: UIViewController{

    
    private var playlists:Playlists?;
    
    @IBOutlet weak var tableView: UITableView!
    
    private let PLAYLIST_TABLEVIEWCELL = "PlaylistTableViewCell";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let uiNib :UINib = UINib(nibName: PLAYLIST_TABLEVIEWCELL, bundle: nil)
        tableView.register(uiNib, forCellReuseIdentifier: PLAYLIST_TABLEVIEWCELL)
        tableView.delegate = self;
        tableView.dataSource = self;
        downloadData();
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadData(){
        let url = URL(string: "https://demo0937961.mockable.io/playlists")!
        
        BaseService.loadDataFromURL(url: url) { data,error in
            if(error != nil){
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title:"Connection failed.", message: nil, preferredStyle: UIAlertControllerStyle.alert)
                    let action_try = UIAlertAction(title: "Try again", style: .cancel) { (action:UIAlertAction) in
                        
                    };
                    alert.addAction(action_try);
                    
                    self.present(alert, animated: true, completion: nil)
                }
                return;
            }
            
            if(data == nil){ return;}
            
            guard let json = try? JSONSerialization.jsonObject(with: (data)!) as? [String : Any] else {
                print("fails to serialize JSON object")
                return
            }
            
            self.playlists = Playlists(json:json!);
            
            
            
            DispatchQueue.main.async {
                  self.tableView.reloadData()
            }
        }
    }
    
    
  

}


extension AllPlaylistViewController: UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(playlists != nil){
            return playlists!.list.count
        }else{
            return 0;
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PLAYLIST_TABLEVIEWCELL)! as! PlaylistTableViewCell;
        let playlist = playlists!.list[indexPath.row];
        
        cell.playlist_name.text = playlist.list_title;
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let playlist = playlists!.list[indexPath.row];
        
        let playlistViewController = PlaylistViewController();
        playlistViewController.playlist = playlist;
        
        self.navigationController?.pushViewController(playlistViewController, animated: true);
        
    }
    
}
