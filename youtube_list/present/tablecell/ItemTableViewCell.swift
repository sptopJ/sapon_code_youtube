//
//  ItemTableViewCell.swift
//  youtube_list
//
//  Created by MacBook on 20/7/2561 BE.
//  Copyright © 2561 sptop. All rights reserved.
//

import UIKit
import SDWebImage
import youtube_ios_player_helper;

class ItemTableViewCell: UITableViewCell ,YTPlayerViewDelegate {

    @IBOutlet weak var thumb_image: UIImageView!
    @IBOutlet weak var item_title: UILabel!
    
     @IBOutlet weak var player: YTPlayerView!
    
    //var isRequirePlay = false;
    @IBOutlet weak var buffering: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func setItem(item:Item){
        self.item_title.text = item.title;
        //cache thumb image.
        self.thumb_image.sd_setImage(with: URL(string: item.thumb), placeholderImage: UIImage(named: ""))
       
        
        //using video_id instead URL request because framework has some problem.
        let video_id = Util.getVideoID(string: item.link);
        
        self.player.delegate = self;
        if(video_id != nil){
            // start buffer
            self.player.load(withVideoId:video_id!);
        }
      
    }
    
    public func playVideo(){
        thumb_image.isHidden = true;
        player.isHidden = false;
        
        player.playVideo();
    }
    
    public func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        self.buffering.isHidden = true;
    }
   
    
}
